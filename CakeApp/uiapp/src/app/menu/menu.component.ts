import { Component, OnInit } from '@angular/core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  faChevronDown = faChevronDown;
  menus: any;

  constructor() { }

  ngOnInit(): void {
    this.setMenuList()
  }

  setMenuList(){
    this.menus = ['Home', 'Cakes', 'Theme Cakes', 'Cake Ingridients', 'Baking Tools', 'Baking Classes']
  }


  showMenu(e, id){
    let el = document.getElementById(id);
    el.style.display = 'block';

  }

  hideMenu(e, id){
    let el = document.getElementById(id);
    el.style.display = 'block';
  }


}
