import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../../services/common.service';
import { faRupeeSign } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-cake-ingridients',
  templateUrl: './cake-ingridients.component.html',
  styleUrls: ['./cake-ingridients.component.scss']
})
export class CakeIngridientsComponent implements OnInit {

  faRupeeSign = faRupeeSign;
  data;

  constructor(private http: HttpClient, private commonService: CommonService) { }

  ngOnInit(): void {
    this.getThemeCakesData();
  }

  getThemeCakesData(){
    let serviceData;
    this.commonService.getThemeCakesData().subscribe(data => {
      serviceData = data;
      this.data = serviceData.product;
    })
  }


}
