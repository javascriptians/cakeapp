import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../../services/common.service';
import { faRupeeSign } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-cakes',
  templateUrl: './cakes.component.html',
  styleUrls: ['./cakes.component.scss']
})
export class CakesComponent implements OnInit {

  faRupeeSign = faRupeeSign;
  cakes

  constructor(private http: HttpClient, private commonService: CommonService) { }

  ngOnInit(): void {
    this.getCakesData()

  }

  getCakesData(){
    let serviceData;
    this.commonService.getCakeData().subscribe(data => {
      serviceData = data;
      this.cakes = serviceData.product;
    })
  }

}
