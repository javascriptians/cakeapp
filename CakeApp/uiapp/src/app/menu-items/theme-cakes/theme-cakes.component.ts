import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../../services/common.service';
import { faRupeeSign } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-theme-cakes',
  templateUrl: './theme-cakes.component.html',
  styleUrls: ['./theme-cakes.component.scss']
})
export class ThemeCakesComponent implements OnInit {

  faRupeeSign = faRupeeSign;
  data: any;

  constructor(private http: HttpClient, private commonService: CommonService) { }

  ngOnInit(): void {
    this.getThemeCakesData();
  }

  getThemeCakesData(){
    let serviceData;
    this.commonService.getThemeCakesData().subscribe(data => {
      serviceData = data;
      this.data = serviceData.product;
    })
  }


}
