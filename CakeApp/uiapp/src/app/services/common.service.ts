import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
import { catchError} from "rxjs/operators"
import { Observable } from "rxjs"

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient) { }

  public getCakeData () {
    return this.http.get('assets/json-data/cakes.json').pipe(
      map(response => response)
    )
  }

  public getThemeCakesData () {
    return this.http.get('assets/json-data/theme-cakes.json').pipe(
      map(response => response)
    )
  }

  public getBakingToolsData () {
    return this.http.get('assets/json-data/baking-tools.json').pipe(
      map(response => response)
    )
  }
  
  public getBakingClassesData() {
    return this.http.get('assets/json-data/baking-classes.json').pipe(
      map(response => response)
    )
  }

}
